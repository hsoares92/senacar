﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Senacar.Data;
using Senacar.Droid;
using SQLite;
[assembly: Xamarin.Forms.Dependency(typeof(SQLite_android))]
namespace Senacar.Droid
{
    class SQLite_android : ISQLite
    {
        private const string arquivoDb = "Senacar.db3";


        public SQLiteConnection conexao()
        {
            var PathDb = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.Path, arquivoDb);

            return new SQLite.SQLiteConnection(PathDb);
        }
    }
}