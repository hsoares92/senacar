﻿using Senacar.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Senacar.Data
{
    class CompraDAO
    {
        readonly SQLiteConnection Conexao;

        public CompraDAO(SQLiteConnection conexao)
        {
            this.Conexao = conexao;
            this.Conexao.CreateTable<Compra>();
        }

        public void Salvar(Compra compra)
        {
            Conexao.Insert(compra);
        }
    }
}
