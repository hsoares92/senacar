﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Senacar.Data
{
    public interface ISQLite
    {
        SQLiteConnection conexao();
    }
}
