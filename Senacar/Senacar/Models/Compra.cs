﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Senacar.Models
{
    class Compra
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }

        public Compra(string nome, string celular, string email)
        {
            this.Nome = nome;
            this.Celular = celular;
            this.Email = email;

        }
    }
}
