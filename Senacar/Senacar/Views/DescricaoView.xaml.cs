﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Senacar.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DescricaoView : ContentPage
    {
        private const int VIDROSELETRICOS = 545;
        private const int TRAVASELETRICAS = 260;
        private const int ARCONDICIONADO = 480;
        private const int CAMERA_RE = 180;
        private const int CAMBIO = 460;
        private const int SUSPENSAO = 380;
        private const int FREIOS = 245;

        public string TextoVidros
        {
            get
            {
                return string.Format("Vidros elétricos - R$ {0}", VIDROSELETRICOS);
            }
        }

        public string Textotravas
        {
            get
            {
                return string.Format("Travas elétricas - R$ {0}", TRAVASELETRICAS);
            }
        }


        public string TextoAr
        {
            get
            {
                return string.Format("Ar Condicionado - R$ {0}", ARCONDICIONADO);
            }
        }


        public string TextoCamera
        {
            get
            {
                return string.Format("Camera de Ré - R$ {0}", CAMERA_RE);
            }
        }


        public string TextoCambio
        {
            get
            {
                return string.Format("Cambio - R$ {0}", CAMBIO);
            }
        }


        public string TextoSuspensao
        {
            get
            {
                return string.Format("Suspensão - R$ {0}", SUSPENSAO);
            }
        }


        public string TextoFreios
        {
            get
            {
                return string.Format("Freios - R$ {0}", FREIOS);
            }
        }

        bool incluiVidros;

        public bool IncluiVidros
        {
            get
            {
                return incluiVidros;
            }
            set
            {
                incluiVidros = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiVidros)
                   // DisplayAlert("Vidros elétricos", "Ativo", "OK");
              //  else
                   // DisplayAlert("Vidros elétricos", "Inativo", "OK");
            }
        }

        bool incluiTravas;

        public bool IncluiTravas
        {
            get
            {
                return incluiTravas;
            }
            set
            {
                incluiTravas = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiTravas)
                    //DisplayAlert("Travas elétricas", "Ativo", "OK");
                //else
                    //DisplayAlert("Travas elétricas", "Inativo", "OK");
            }
        }

        bool incluiAr;

        public bool IncluiAr
        {
            get
            {
                return incluiAr;
            }
            set
            {
                incluiAr = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiAr)
                   // DisplayAlert("Ar Condicionado", "Ativo", "OK");
               // else
                   // DisplayAlert("Ar Condicionado", "Inativo", "OK");
            }
        }

        bool incluiCamera;

        public bool IncluiCamera
        {
            get
            {
                return incluiCamera;
            }
            set
            {
                incluiCamera = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiCamera)
                   // DisplayAlert("Camera de Ré", "Ativo", "OK");
               // else
                  //  DisplayAlert("Camera de Ré", "Inativo", "OK");
            }
        }

        bool incluiCambio;

        public bool IncluiCambio
        {
            get
            {
                return incluiCambio;
            }
            set
            {
                incluiCambio = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiCambio)
                //    DisplayAlert("Cambio", "Ativo", "OK");
                //else
                //    DisplayAlert("Cambio", "Inativo", "OK");
            }
        }

        bool incluiSuspensao;

        public bool IncluiSuspensao
        {
            get
            {
                return incluiSuspensao;
            }
            set
            {
                incluiSuspensao = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiSuspensao)
                //    DisplayAlert("Suspensão", "Ativo", "OK");
                //else
                //    DisplayAlert("Suspensão", "Inativo", "OK");
            }
        }

        bool incluiFreios;

        public bool IncluiFreios
        {
            get
            {
                return incluiFreios;
            }
            set
            {
                incluiFreios = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiFreios)
                //    DisplayAlert("Freios", "Ativo", "OK");
                //else
                //    DisplayAlert("Freios", "Inativo", "OK");
            }
        }


        public string ValorTotal
        {
            get
            {
                return string.Format("Valor Total: R$: {0}", Veiculo.Preco + (IncluiVidros ? VIDROSELETRICOS : 0)
                    + (IncluiTravas ? TRAVASELETRICAS : 0)
                    + (IncluiAr ? ARCONDICIONADO : 0)
                    + (IncluiCamera ? CAMERA_RE : 0)
                    + (IncluiCambio ? CAMBIO : 0)
                    + (IncluiSuspensao ? SUSPENSAO : 0)
                    + (IncluiFreios ? FREIOS : 0));


            }
        }

        public Veiculos Veiculo { get; set; }
        public DescricaoView(Veiculos veiculos)
        {
            InitializeComponent();
            this.Title = veiculos.Nome;
            this.Veiculo = veiculos;
            this.BindingContext = this;
        }

        private void ButtonProximo_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CompraView(this.Veiculo));
        }
    }
}