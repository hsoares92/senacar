﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Senacar.Views
{
    public class Veiculos
    {
        public string Nome { get; set; }
        public string Nome2 { get; set; }
        public float Preco { get; set; }

        public string PrecoFormatado
        {
            get { return string.Format("R$ {0}", Preco); }
        }
    }
    public partial class ListaView : ContentPage
    {
        public List<Veiculos> Veiculos { get; set; }


        public ListaView()
        {
                InitializeComponent();

            this.Veiculos = new List<Veiculos>
            {
                new Veiculos {Nome="Ford", Nome2= "GT", Preco= 900000},
                new Veiculos {Nome="Ford", Nome2= "Mustang", Preco= 400000},
                new Veiculos {Nome="Dodge", Nome2= "Ram", Preco= 80000},
                new Veiculos {Nome="Nissan", Nome2= "Skyline", Preco= 200000},
                new Veiculos {Nome="Fiat", Nome2= "150", Preco= 2000},
            };

            this.BindingContext = this;

            //ListviewVeiculos.ItemsSource = this.Veiculos;

        }

        private void ListviewVeiculos_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var veiculo = (Veiculos)e.Item;

            Navigation.PushAsync(new DescricaoView(veiculo));


            //DisplayAlert("Veiculo", string.Format("Voce selecionou a Marca '{0}'. Modelo: {1}. Valor {2}", veiculo.Nome, veiculo.Nome2, veiculo.PrecoFormatado), "OK");
        }
    }
}
